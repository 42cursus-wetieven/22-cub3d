/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub_data.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/09 16:39:36 by wetieven          #+#    #+#             */
/*   Updated: 2021/05/13 11:24:10 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

typedef enum e_cub_data {
	RES,
	NOR,
	SOU,
	WES,
	EAS,
	SPR,
	FLO,
	CEI,
	END,
	MAP,
}	t_cub_data;

typedef struct s_resol {
	unsigned int	wid,
	unsigned int	hei,
}	t_resol;

typedef struct s_cub {
	char		*flag,
	t_parser	fct,
	void		*ctnt,
}	t_cub;

typedef struct t_map {
	unsigned int	width;
	char			*grid;
}	t_map;

typedef t_error	(*t_parser)(char **line, t_cub_datum elem, t_cub_data *cub);
