/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub_data.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/09 16:34:24 by wetieven          #+#    #+#             */
/*   Updated: 2021/05/13 11:58:34 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "cub_parsing.h"
#include "cub_data.h"

t_parser	*switchboard(char **line, t_cub_data *elem, t_cub *cub)
{
	static int		data_count = 8;
	static t_cub	cub[] = {
		[RES] = {.flag = "R", .fct = &resol, .ctnt = NULL},
		[NOR] = {.flag = "NO", .fct = &textr, .ctnt = NULL},
		[SOU] = {.flag = "SO", .fct = &textr, .ctnt = NULL},
		[WES] = {.flag = "WE", .fct = &textr, .ctnt = NULL},
		[EAS] = {.flag = "EA", .fct = &textr, .ctnt = NULL},
		[SPR] = {.flag = "S", .fct = &textr, .ctnt = NULL},
		[FLO] = {.flag = "F", .fct = &color, .ctnt = NULL},
		[CEI] = {.flag = "C", .fct = &color, .ctnt = NULL},
		[END] = {NULL, .fct = &completion, .ctnt = &data_count},
		[MAP] = {NULL, .fct = &map_builder, .ctnt = NULL},
	};

	elem = 0;
	while (cub[elem]->flag && ft_strncmp(cub[elem]->flag, **line, 2))
		elem++;
	if (cub[elem]->flag && !cub[elem]->ctnt)
	{
		(*line) += ft_strlen(cub[elem]->flag);
		data_count--;
		return (cub[elem]->fct);
	}
	else
		return (&completion);
}
