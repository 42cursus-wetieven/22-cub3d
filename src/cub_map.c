/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub_map.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/09 17:32:58 by wetieven          #+#    #+#             */
/*   Updated: 2021/05/13 11:22:43 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "cub_data.h"

t_error	map_building(t_map map)
{
	unsigned int	i;
	unsigned int	line_len;
	unsigned int	map_width;

	if (fd_to_vctr(map, fd, 4096) != clear)
		return (mem_alloc);
	i = 0;
	line_len = 0;
	while (i < raw_map->entries)
	{
		if (raw_map->data[i] == '\n')
		{
			if (line_len > map_width)
				map_width = line_len;
			line_len = 0;
		}
		i++;
	}
}
