/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub_parsing.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/05 09:42:56 by wetieven          #+#    #+#             */
/*   Updated: 2021/05/13 12:11:48 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "cub_process.h"
#include "cub_data.h"
#include "cub_map.h"

t_error	completion(char **line, t_cub_data elem, t_cub *cub)
{
	if (!cub[END]->ctnt)
		return (clear);
	else if (cub[elem]->flag && cub[elem]->ctnt)
		return (parse);
	else
		return (incomplete);
}

t_error	resol(char **line, t_cub_data elem, t_cub *cub)
{
	t_resol	*resol;

	if (!alloc_safe(resol, sizeof(t_resol), 1, heap_top())
		return (mem_alloc);
	resol->wid = ptr_atoi(line);
	if (resol->wid <= 0)
		return (parse);
	resol->hei = ptr_atoi(line);
	if (resol->hei <= 0)
		return (parse);
	cub[elem]->(t_resol *)ctnt = resol;
	return (clear);
}

t_error	textr(char **line, t_cub_data elem, t_cub *cub)
{
	t_vctr	*path;

	path = vctr_init(sizeof(char), 64);
	if (!path)
		return (mem_alloc);
	while (ft_isspace(**line))
		(*line)++;
	while (**line)
		vctr_push(path, *((*line)++));
	vctr_push(path, "\0");
	cub[elem]->(char *)ctnt = path->data;
	return (clear);
}

t_error	color(char **line, t_cub_data elem, t_cub *cub)
{
	int	*trgb;
	int buf;
&txtrs
	if (!alloc_safe(trgb, sizeof(int), 1, heap_top())
		return (mem_alloc);
	buf = ptr_atoi(line);
	if (buf < 0 || buf > 255)
		return (parse);
	*trgb = buf << 16;
	buf = ptr_atoi(line);
	if (buf < 0 || buf > 255)
		return (parse);
	*trgb = buf << 8;
	buf = ptr_atoi(line);
	if (buf < 0 || buf > 255)
		return (parse);
	*trgb = buf;
	cub[elem]->(int *)ctnt = trgb;
	return (clear);
}

t_error	parsing(char *cub_path)
{
	int			fd;
	char		*line;
	t_cub		*cub;
	t_cub_data	elem;
	t_error		error;

	if (fd_opener(cub_path, &fd))
		return (invalid_fd);
	while (get_next_line(fd, &line) > 0)
	{
		error = (switchboard(&line, &elem, cub))(&line, &elem, cub);
		free(line);
		if (error)
			return (cub_shutdown(error, elem));
		if (elem == END && error == clear)
			break ;
	}
	if (cub[END]->ctnt != 0)
		return(cub_shutdown(parse, END));
	map_building(map);
	return (fd_killer(&fd));
}
